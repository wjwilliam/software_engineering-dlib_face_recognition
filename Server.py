# !/usr/bin/python
# coding=utf-8
# Author: JohnZero
# Email: johnzero74@qq.com
# Time: 2022/6/17 19:28
# Description: https://blog.csdn.net/u013008795/article/details/92642840

import sys
sys.path.append('src')
import logging, socketserver, socket, threading, pickle
from traceback import print_exc
from src.message import util, Handler

util.server_tag=True
logging.basicConfig(format="%(asctime)s %(thread)d %(threadName)s %(message)s",stream=sys.stdout,level=logging.INFO)
log = logging.getLogger()

class Server(socketserver.BaseRequestHandler):
    lock = threading.Lock()
    clients = {}

    def setup(self):
        super().setup()
        self.event = threading.Event()
        with self.lock:
            self.clients[self.client_address] = [self.request, None]
        log.info("新加入了一个连接{}".format(self.client_address))

    def handle(self):
        super().handle()
        sock:socket.socket = self.request
        while not self.event.is_set():
            try:
                data = Handler.recv(sock)
            except Exception as e:
                # log.error(e)
                data = b''
            # log.info(data)
            if data == b'':
                break
            with self.lock:
                try:
                    data = pickle.loads(data)
                    if data[0] == 'login':
                        res = exist = False
                        id_number = data[1][0]
                        for i in self.clients.values():
                            if i[1] == id_number:
                                exist = True
                                break
                        if not exist:
                            res = True
                            self.clients[self.client_address][1] = id_number
                    elif data[0] == 'logout':
                        res = self.clients[self.client_address][1] = None
                    else:
                        res = getattr(Handler, data[0])(*data[1], **data[2])
                    Handler.send(self.clients[self.client_address][0], pickle.dumps(res))
                except:
                    self.clients.pop(self.client_address)
                    print_exc()

    def finish(self):
        super().finish()
        self.event.set()
        with self.lock:
            if self.client_address in self.clients:
                self.clients.pop(self.client_address)
        self.request.close()
        log.info("{}退出了".format(self.client_address))

if __name__ == "__main__":
    server = socketserver.ThreadingTCPServer(("127.0.0.1",6666), Server)
    server.daemon_threads = True  #设置所有创建的线程都为Daemon线程
    threading.Thread(target=server.serve_forever,name="server",daemon=True).start()
    while True:
        CMD = input(">>>")
        if CMD.lower() == "exit":
            server.shutdown() # serve_forever循环停止
            server.server_close()
            break
        logging.info(threading.enumerate())
