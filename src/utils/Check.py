from src.utils.MyMd5 import MyMd5
from message import Handler

def check_user_id(user_id):

    # if not user_id.isdigit() or len(user_id) > 100:
    if len(user_id) > 100:
        return False
    return True

def check_user_pwd(user_pwd):
    # if len(user_pwd) < 6 or len(user_pwd) > 13:
    if len(user_pwd) > 13:
        return False
    return True

def verifye_pwd(user_id,user_pwd):
    user = Handler.verify_pwd(user_id)
    if len(user) == 0:
      return False
    elif len(user) == 1:
        item = user[0]
        pass_word = MyMd5().create_md5(user_pwd, item["salt"])
        if pass_word == item["password"]:
            return item["id_number"]
        else:
            return False
    else:
        return False
   
# QMessageBox.information(parent, 'Information', '警告 username or Password')
