import os,datetime,cv2
from src.message import Handler

class  studentlog():
    def __init__(self, vector, img, ifLog = True):
        # 用户信息
        self.item = Handler.student_log(ifLog, vector, img)
        path = f'img_information/student/{self.item["id_number"]}'
        if not os.path.exists(path):  # 判断是否存在文件夹如果不存在则创建为文件夹
            os.makedirs(path)
        cv2.imwrite(path + "/" + str(datetime.datetime.now().strftime("%Y-%m-%d-%H-%M")) + ".jpg", img)
