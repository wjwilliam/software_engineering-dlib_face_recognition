import multiprocessing

from PyQt5.QtWidgets import QWidget, QLabel,QVBoxLayout,QHBoxLayout,QMessageBox, QPushButton
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt
from src.process.Capture import Capture
from PyQt5.QtGui import QImage,QPixmap,QIcon
from src.model.Face import StudentRgFace
import cv2
from src.utils.GlobalVariable import models
from PyQt5.QtWidgets import QGroupBox
import datetime, os

class CapFace(QWidget):
    emit_face = pyqtSignal(str)

    def __init__(self) -> None:
        super().__init__()
        self.setWindowTitle("获取人脸")
        self.setWindowIcon(QIcon("resources/人脸识别.png"))

        self.Hlayout = QHBoxLayout()
        self.Vlayout = QVBoxLayout(self)

        self.btn1 = QPushButton(objectName="GreenButton")
        self.btn1.setText("拍照")
        self.btn1.setIcon(QIcon("./resources/人脸识别.png"))
        self.btn1.setFlat(True)
        self.btn1.clicked.connect(self.take_photo)

        self.groupbox = QGroupBox(self)
        self.groupbox.setLayout(self.Hlayout)
        self.label1 = QLabel()
        self.label1.setAlignment(Qt.AlignHCenter)
        self.label2 = QLabel(self)
        self.Hlayout.addWidget(self.label1)
        self.Vlayout.addWidget(self.groupbox)
        self.Vlayout.addWidget(self.label2)
        self.Vlayout.addWidget(self.btn1)
        self.setLayout(self.Vlayout)
        self.groupbox.setFixedSize(480, 35)
        # self.groupbox.hide()
        self.resize(480, 600)
        self.setWindowModality(Qt.ApplicationModal)#
        self.face_rg = StudentRgFace()
        self.capture = Capture()
        self.capture.cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        self.capture.emit_img.connect(self.set_normal_img)
        self.capture.start()
        self.list_img = []
        self.share = multiprocessing.Value("f", 0.4)  # 父子进程间的同步
        self.show()

    def take_photo(self):
        rgbImage = cv2.cvtColor(self.capture.frame, cv2.COLOR_BGR2RGB)
        gray = cv2.cvtColor(rgbImage, cv2.COLOR_RGB2GRAY)
        location_faces = models.detector(gray)
        if len(location_faces) == 1:
            tmp = './tmp'
            if not os.path.exists(tmp):
                os.makedirs(tmp)
            path = tmp + "/" + str(datetime.datetime.now().strftime("%Y-%m-%d-%H-%M")) + ".jpg"
            cv2.imwrite(path, self.capture.frame)
            self.capture.close()
            QMessageBox.information(self, '提醒', '录入成功！', QMessageBox.Yes)
            self.emit_face.emit(path)
            self.close
        else:
            QMessageBox.warning(self, '警告', '未识别出人脸，请重新拍摄', QMessageBox.Yes)

    def closeEvent(self, event):
        self.capture.close()
        super().closeEvent(event)

    @pyqtSlot(list,QImage)
    def set_normal_img(self, list_,img):
        self.capture.frame = list_[0]#待识别帧
        self.label2.setPixmap(QPixmap.fromImage(img))#设置图片
        #QPixmap.fromImage(img).scaled(self.label2.size(), Qt.KeepAspectRatio))#图片跟随qlabel大小缩放
        self.label2.setScaledContents(True)#qlabel2自适应图片大小