# !/usr/bin/python
# coding=utf-8
# Author: JohnZero
# Email: johnzero74@qq.com
# Time: 2022/6/18 14:46
# Description:

import os, sys, socket, struct, datetime, cv2, pickle
from src.message import Client, util
from src.DB.Database import Database

func_name=None
filter=['handler', 'func_name', 'filter', 'request_func', 'send', 'recv']

class Handler:
    def __getattribute__(self, item):
        global func_name
        if not util.server_tag and item not in filter:
            func_name = item
            return request_func
        return globals()[item]

handler = Handler()
sys.modules[__name__] = handler

def request_func(*args, **kwargs):
    send(Client.client, pickle.dumps((func_name, args, kwargs)))
    return pickle.loads(recv(Client.client))

def send(sock:socket.socket, data:bytes):
    sock.send(struct.pack('i', len(data)))
    sock.send(data)

def recv(sock:socket.socket):
    return sock.recv(struct.unpack('i', sock.recv(4))[0])

def login(id_number):
    return False

def logout():
    pass

def check_register_id(user_name):
    admin = Database()
    user = admin.c.execute("select id_number from student where id_number = {}".format(user_name)).fetchall()
    return user

def register(id_number,user_name,gender,password,img_path,salt,vector,count):
    admin = Database()
    admin.c.execute(
        "INSERT INTO student (id_number,user_name,gender,password,img_path,salt,vector, count) \
    VALUES (?, ?, ?, ?, ?, ?, ?, ?)", (id_number,user_name,gender,password,img_path,salt,vector,count))
    admin.conn.commit()
    admin.conn.close()

def verify_pwd(user_id):
    admin = Database()
    user = admin.c.execute("select id_number, salt, password, gender from student where id_number = {}".format(user_id)).fetchall()
    return user

def load_vectors():
    student = Database()
    list_vector = [pickle.loads(i["vector"]) for i in student.c.execute("SELECT vector from student")] # 查询数据库中所有人脸编码
    student.conn.close()
    return list_vector

def student_log(ifLog, vector, img):
    database = Database()
    # 用户信息
    try:
        item = database.c.execute("SELECT * from student where vector = ?",(vector,)).fetchall()[0] # 取出返回所有数据，fetchall返回类型是[()]
    except:
        return
    if ifLog:
        database.c.execute("INSERT INTO student_log_time (id_number,gender,log_time,type ) VALUES (?, ?,?,?)",
            (item["id_number"], item["gender"], datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"), 1))
        database.conn.commit()
        # 向数据库插入识时照片
        path = item["img_path"]
        if not os.path.exists(path):  # 判断是否存在文件夹如果不存在则创建为文件夹
            os.makedirs(path)
        cv2.imwrite(path + "/" + str(datetime.datetime.now().strftime("%Y-%m-%d-%H-%M")) + ".jpg", img)
        if item["count"] == None:
            cout = 1
            database.c.execute("UPDATE student SET count = {0} WHERE id_number = {1}".format(cout, item["id_number"]), )
            database.conn.commit()
        else:
            cout =item["count"] + 1
            database.c.execute("UPDATE student SET count = {0} WHERE id_number = {1}".format(cout, item["id_number"]), )
            database.conn.commit()
    database.conn.close()
    return item

def delete_id(id):
    data = Database()
    data.delete(id)

def update1(id_number, user_name, gender, id):
    data = Database()
    sql = "UPDATE student SET id_number = {0},user_name = '{1}',gender = {2} WHERE id_number = {3}".format(id_number, user_name, gender, id)
    data.c.execute(sql)
    data.conn.commit()
    data.conn.close()

def update2(id_number, user_name, gender, vector):
    data = Database()
    data.c.execute("update student set id_number= ?,user_name = ?,gender = ? ,vector = ? where id_number = {0}"
                   .format(id), (id_number, user_name, gender, vector))
    data.conn.commit()
    data.conn.close()

def get_id_info(id_number):
    return Database().c.execute("select id_number,user_name,gender,count from student where id_number ={0}".format(id_number)).fetchall()

def get_id_info1(id_number):
    return Database().c.execute("select id_number,user_name,gender from student where id_number = {}".format(id_number)).fetchall()

def get_id_log(id_number):
    return Database().c.execute("select rowid,id_number,log_time from student_log_time where id_number = {0} order by log_time desc".format(id_number)).fetchall()

def get_all_log():
    return Database().c.execute("select rowid,id_number,log_time from student_log_time order by log_time desc").fetchall()

def update_id_vector(id_number, vector):
    database = Database()
    database.c.execute("update student set vector = ? where id_number = {0}".format(id_number), (vector,))
    database.conn.commit()
    database.conn.close()

def get_all_students():
    return Database().c.execute("select id_number,password from student").fetchall()

def get_all_students1():
    return Database().c.execute("select id_number,user_name,gender from student").fetchall()

def get_data(date1, date2, sql, sql_female, sql_male):
    database = Database()
    reuslt = database.c.execute(sql.format(date1, date2)).fetchall()
    result1 = reuslt[0]['count(id_number)']
    reuslt = database.c.execute(sql_female.format(date1, date2)).fetchall()
    result2 = reuslt[0]['count(id_number)']
    reuslt = database.c.execute(sql_male.format(date1, date2)).fetchall()
    result3 = reuslt[0]['count(id_number)']
    return result1, result2, result3

def delete_student_log(id):
    database = Database()
    database.c.execute("delete from student_log_time where rowid  = {0}".format(id)).fetchall()
    database.conn.commit()
    database.conn.close()

def get_pwd(id_number):
    return Database().c.execute("select password ,salt from student where id_number = {0}".format(id_number)).fetchone()

def update_pwd(id_number, new_pass_word):
    database = Database()
    database.c.execute("update student set password = ? where id_number = {0}".format(id_number), (new_pass_word,))
    database.conn.commit()
    database.conn.close()
