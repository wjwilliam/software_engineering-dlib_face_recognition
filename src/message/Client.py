# !/usr/bin/python
# coding=utf-8
# Author: JohnZero
# Email: johnzero74@qq.com
# Time: 2022/6/17 19:53
# Description: https://blog.csdn.net/qq_44278941/article/details/120850506

import socket

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # 创建一个tcp/ip协议的套接字

def connect(host='localhost', port=6666):
    global client
    try:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # 创建一个tcp/ip协议的套接字
        client.connect((host, port))
    except:
        pass

def closed():
    return getattr(client, '_closed')

def close():
    client.close()

def reconnect():
    close()
    connect()

connect()
