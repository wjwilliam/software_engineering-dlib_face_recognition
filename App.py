import sys
sys.path.append('src')
from PyQt5.QtWidgets import QApplication
from src import Ui
from test import StyleSheet

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyleSheet(StyleSheet)    
    ui = Ui.Ui()
    app.exec_()
